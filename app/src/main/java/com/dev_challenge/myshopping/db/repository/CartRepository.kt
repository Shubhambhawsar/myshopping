package com.dev_challenge.myshopping.db.repository

import android.util.Log
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.dao.MyCartDao
import com.dev_challenge.myshopping.db.entities.CartProduct

class CartRepository(private val cartDao: MyCartDao, private val resultListener: ResultListener) {
    val allProductsInCart = cartDao.getAllProduct()

    suspend fun addProductToCart(product: CartProduct){
        val ob = cartDao.addProductToCart(product)
        Log.e(CartRepository::class.simpleName, "addProductToCart: "+ob )
        resultListener.onSuccess(null, "add")
    }
    suspend fun updateProductOfCart(product: CartProduct){
        cartDao.updateProductOfCart(product)
        resultListener.onSuccess(null, "update")
    }
    suspend fun deleteProductFromCart(product: CartProduct){
        val ob = cartDao.deleteProductFromCart(product)
        Log.e(CartRepository::class.simpleName, "deleteProductFromCart: "+ob )
        resultListener.onSuccess(null, "delete")
    }
    suspend fun deleteAllCartProducts(){
        cartDao.deleteAllCartProducts()
    }
}