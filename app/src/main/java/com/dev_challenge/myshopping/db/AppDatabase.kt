package com.dev_challenge.myshopping.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dev_challenge.myshopping.db.dao.MyCartDao
import com.dev_challenge.myshopping.db.dao.ProductDao
import com.dev_challenge.myshopping.db.entities.CartProduct
import com.dev_challenge.myshopping.db.entities.Product

@Database(
    entities =  [Product::class, CartProduct::class],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun getProductDao(): ProductDao

    abstract fun getMyCartProductDao(): MyCartDao

    companion object{
        @Volatile
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase{
            synchronized(this){
                var instance = INSTANCE
                if (instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "my-shopping.db"
                    ).build()
                }
                return instance
            }
        }
    }
}