package com.dev_challenge.myshopping.ui.cart

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dev_challenge.myshopping.R
import com.dev_challenge.myshopping.databinding.RowCartProductsBinding
import com.dev_challenge.myshopping.db.entities.CartProduct

class CartProductListAdapter( myCartActivity: MyCartActivity, productList: List<CartProduct>) : RecyclerView.Adapter<CartProductListAdapter.MyViewHolder>() {
    var cartProductList: List<CartProduct> = productList
    var activity: MyCartActivity = myCartActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = DataBindingUtil.inflate<RowCartProductsBinding>(LayoutInflater.from(parent.context), R.layout.row_cart_products, parent, false);
        return MyViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return cartProductList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.apply {
            txvName.setText(cartProductList[position].name)

            txvQuantity.setText(cartProductList[position].count.toString())

            val total = (cartProductList[position].count * cartProductList[position].price)
            txvTotalItemPrice.setText(total.toString() + " \u20A8")

            ivBtnAddItem.setOnClickListener {
                activity.updateCartProduct("add", position)
            }

            idBtnSubtractItem.setOnClickListener {
                if (cartProductList[position].count>1)
                    activity.updateCartProduct("sub", position)
                else {
                    activity.deleteCartProduct(position)
                }
            }
        }
    }

    class MyViewHolder(rowBinding: RowCartProductsBinding) : RecyclerView.ViewHolder(rowBinding.root) {
        var binding = rowBinding
    }
}
