package com.dev_challenge.myshopping.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.dev_challenge.myshopping.db.entities.Product

@Dao
interface ProductDao {

    @Insert(onConflict = REPLACE)
    suspend fun addProduct(product: Product):Long

    @Update
    suspend fun editProduct(product: Product)

    @Delete
    suspend fun deleteProduct(product: Product)

    @Query("SELECT * FROM Product")
    fun getAllProduct(): LiveData<List<Product>>

    @Query("DELETE FROM Product")
    suspend fun deleteAllProduct()

}