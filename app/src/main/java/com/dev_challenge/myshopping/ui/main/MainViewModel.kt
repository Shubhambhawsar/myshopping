package com.dev_challenge.myshopping.ui.main

import android.app.Application
import android.content.Context
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dev_challenge.myshopping.db.entities.Product
import com.dev_challenge.myshopping.db.repository.ProductRepository
import androidx.lifecycle.viewModelScope
import com.dev_challenge.myshopping.db.AppDatabase
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.entities.CartProduct
import com.dev_challenge.myshopping.db.repository.CartRepository
import com.example.myapplication.showToast
import kotlinx.coroutines.launch

class MainViewModel(application: Application, private val context: Context, private val resultListener: ResultListener) : AndroidViewModel(application) {
    val productRepository = ProductRepository(AppDatabase.getInstance(context).getProductDao(), resultListener)
    val cartRepository = CartRepository(AppDatabase.getInstance(context).getMyCartProductDao(), resultListener)

    val allProductsInDb = productRepository.allProducts
    val allProductsInCart = cartRepository.allProductsInCart

    val prodName = MutableLiveData<String>()
    val prodPrice = MutableLiveData<String>()
    val prodQuantity = MutableLiveData<String>()
    val prodOffer = MutableLiveData<String>()

    fun addNewProduct(view: View, selectedImagePath: String?) {
        if (prodName.value.isNullOrEmpty())
            context.showToast("Enter Product Name")
        else if (prodPrice.value.isNullOrEmpty())
            context.showToast("Enter Product Price")
        else if (prodQuantity.value.isNullOrEmpty())
            context.showToast("Enter total available stock count")
        else if (prodOffer.value.isNullOrEmpty()) {
            insertProduct(Product(selectedImagePath!!.toString(), prodName.value!!, prodPrice.value!!.toInt(), prodQuantity.value!!.toInt(), prodQuantity.value!!.toInt(), prodQuantity.value!!.toInt(), ""))
            setEmptyValue()
        } else{
            insertProduct(Product(selectedImagePath!!, prodName.value!!, prodPrice.value!!.toInt(), prodQuantity.value!!.toInt(), prodQuantity.value!!.toInt(), prodQuantity.value!!.toInt(), prodOffer.value!!))
            setEmptyValue()
        }
    }

    fun setEmptyValue(){
        prodName.value = ""
        prodPrice.value = ""
        prodQuantity.value = ""
        prodOffer.value = ""
    }

    private fun insertProduct(product: Product){
        viewModelScope.launch {
            productRepository.addProduct(product)
        }
    }

    fun updateProduct(product: Product){
        viewModelScope.launch {
            productRepository.updateProduct(product)
        }
    }

    fun deleteProduct(product: Product){
        viewModelScope.launch {
            productRepository.deleteProduct(product)
        }
    }


    fun addProductToCart(product: CartProduct){
        viewModelScope.launch {
            cartRepository.addProductToCart(product)
        }
    }

    fun updateProductOfCart(product: CartProduct){
        viewModelScope.launch {
            cartRepository.updateProductOfCart(product)
        }
    }

    fun deleteProductFromCart(product: CartProduct){
        viewModelScope.launch {
            cartRepository.deleteProductFromCart(product)
        }
    }

}