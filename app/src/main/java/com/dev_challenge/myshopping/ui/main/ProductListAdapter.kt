package com.dev_challenge.myshopping.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev_challenge.myshopping.R
import com.dev_challenge.myshopping.databinding.RowProductsBinding
import com.dev_challenge.myshopping.db.entities.Product

class ProductListAdapter(context: Context?, mainActivity: MainActivity, productList: List<Product>) : RecyclerView.Adapter<ProductListAdapter.MyViewHolder>() {
    var mContext = context!!
    var productArrayList: List<Product> = productList
    var activity: MainActivity = mainActivity

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = DataBindingUtil.inflate<RowProductsBinding>(LayoutInflater.from(parent.context), R.layout.row_products, parent, false);
        return MyViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return productArrayList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.apply {
            txvName.setText(productArrayList[position].name)
            txvPrice.setText(productArrayList[position].price.toString() + " \u20A8")
            txvAvailable.setText(productArrayList[position].availability.toString() + " More available")
            if (productArrayList[position].image.length>10)
                Glide.with(mContext).load(productArrayList[position].image).into(ivImage)

            txvAddToCart.setOnClickListener {
                activity.onClickAddItemToCart(position)
            }

            ivBtnClose.setOnClickListener {
                activity.onClickRemoveItemToCart(position)
            }
        }
    }

    class MyViewHolder(rowBinding: RowProductsBinding) : RecyclerView.ViewHolder(rowBinding.root) {
        var binding = rowBinding
    }
}
