package com.dev_challenge.myshopping.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CartProduct(
    var productId: Int?,
    var image: String,
    var name: String,
    var price: Int,
    var count: Int,
    var offer: String,
    @PrimaryKey(autoGenerate = true) val id: Int? = null
)