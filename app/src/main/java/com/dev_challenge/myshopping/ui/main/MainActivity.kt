package com.dev_challenge.myshopping.ui.main

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.dev_challenge.myshopping.FileUtils
import com.dev_challenge.myshopping.R
import com.dev_challenge.myshopping.databinding.ActivityMainBinding
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.entities.Product
import com.dev_challenge.myshopping.ui.cart.MyCartActivity
import com.example.myapplication.showToast
import com.example.myapplication.ui.MainViewModelFactory
import com.dev_challenge.myshopping.db.entities.CartProduct

class MainActivity : AppCompatActivity(), ResultListener {
    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    var productList  = ArrayList<Product>()
    lateinit var cartProductList: List<CartProduct>
    lateinit var adapter : ProductListAdapter
    val GALLERY_REQUEST_CODE = 1
    var selectedImagePath: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this, MainViewModelFactory(application, this, this)).get(MainViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        adapter = ProductListAdapter(this, this, productList);
        binding.apply {
            rvItemList.layoutManager = GridLayoutManager(this@MainActivity, 2)
            rvItemList.adapter = adapter
            txvBtnAddProduct.setOnClickListener {
                if (cslAddItem.isVisible)
                    viewModel?.addNewProduct(it, selectedImagePath)
                else
                    cslAddItem.visibility = VISIBLE
            }
            ivBtnCart.setOnClickListener {
                startActivity(Intent(this@MainActivity, MyCartActivity::class.java))
                finish()
            }

            ivProductImage.setOnClickListener {
                openGalleryForImage()
            }
        }
        addProductnCartListObservers()
    }

    private fun addProductnCartListObservers() {
        viewModel.allProductsInDb.observe(this, Observer {
            productList.clear()
            it.map {
                productList.add(it)
            }
            adapter.notifyDataSetChanged()
        })

        viewModel.allProductsInCart.observe(this, Observer {
            binding.txvCartItemCount.setText(it.size.toString())
            cartProductList = it
        })
    }

    private fun showAlertDialog(product: Product) {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this@MainActivity)
        alertDialog.setMessage("Do you want to delete "+product.name + " ?")
        alertDialog.setPositiveButton(
                "yes"
        ) { _, _ ->
            viewModel.deleteProduct(product)
        }
        alertDialog.setNegativeButton(
                "No"
        ) { _, _ -> }
        val alert: AlertDialog = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    fun onClickAddItemToCart(position: Int) {
        val item = productList[position]
        lateinit var cartProduct : CartProduct
        var isAlreadyInCart = false
        cartProductList.map {
            if (it.productId == item.id) {
                cartProduct = it
                cartProduct.count = ( cartProduct.count + 1 )
                isAlreadyInCart = true
            }
        }
        if (item.availability > 0) {
            if (isAlreadyInCart) {
                viewModel.updateProductOfCart(cartProduct)
                updateProductAvailabiliy("add", item)
            } else {
                viewModel.addProductToCart(CartProduct(item.id, item.image, item.name, item.price, 1, item.offer))
                updateProductAvailabiliy("add", item)
            }
        } else {
            showToast("Sorry no more item available in stock")
        }
    }

    fun onClickRemoveItemToCart(position: Int) {
        showAlertDialog(productList[position])
    }

    private fun updateProductAvailabiliy(onAction:String, item: Product) {
        if (onAction.equals("add"))
            item.availability = (item.availability - 1)
        else if (onAction.equals("sub"))
            item.availability = (item.availability + 1)

        viewModel.updateProduct(item)
    }

    override fun onSuccess(list: List<Product>?, requestType: String) {
        if(requestType.equals("Product")) {
            showToast("Product added successfully")
            selectedImagePath = ""
            binding.cslAddItem.visibility = GONE
            binding.ivProductImage.setImageDrawable(resources.getDrawable(R.drawable.ic_image))
        } else if (requestType.equals("Cart")) {
            showToast("Product added to cart ")
        }
    }

    override fun onFailure(message: String) {
        TODO("Not yet implemented")
    }

    private fun openGalleryForImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY_REQUEST_CODE){
            binding.ivProductImage.setImageURI(data?.data) // handle chosen image
            selectedImagePath = FileUtils.getPath(this, data?.data)
        }
    }
}