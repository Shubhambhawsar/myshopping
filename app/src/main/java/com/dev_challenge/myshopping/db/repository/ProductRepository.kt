package com.dev_challenge.myshopping.db.repository

import android.util.Log
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.dao.ProductDao
import com.dev_challenge.myshopping.db.entities.Product

class ProductRepository(private val productDao: ProductDao, private val resultListener: ResultListener) {
    val allProducts = productDao.getAllProduct()

    suspend fun addProduct(product: Product){
        var ob = productDao.addProduct(product)
        Log.e(ProductRepository::class.simpleName, "addProduct done: "+ob )
        resultListener.onSuccess(null, "Product")
    }
    suspend fun updateProduct(product: Product){
        productDao.editProduct(product)
        resultListener.onSuccess(null, "Product")
    }
    suspend fun deleteProduct(product: Product){
        productDao.deleteProduct(product)
    }
    suspend fun deleteAllProduct(){
        productDao.deleteAllProduct()
    }
}