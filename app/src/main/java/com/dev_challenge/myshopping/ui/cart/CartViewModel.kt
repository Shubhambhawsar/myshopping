package com.dev_challenge.myshopping.ui.cart

import android.app.Application
import android.content.Context
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.dev_challenge.myshopping.db.entities.Product
import com.dev_challenge.myshopping.db.repository.ProductRepository
import androidx.lifecycle.viewModelScope
import com.dev_challenge.myshopping.db.AppDatabase
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.entities.CartProduct
import com.dev_challenge.myshopping.db.repository.CartRepository
import com.example.myapplication.showToast
import kotlinx.coroutines.launch

class CartViewModel(application: Application, private val context: Context, private val resultListener: ResultListener) : AndroidViewModel(application) {
    val cartRepository = CartRepository(AppDatabase.getInstance(context).getMyCartProductDao(), resultListener)
    val productRepository = ProductRepository(AppDatabase.getInstance(context).getProductDao(), resultListener)

    val allProductsInDb = productRepository.allProducts
    val allProductsInCart = cartRepository.allProductsInCart

    fun updateProduct(product: Product){
        viewModelScope.launch {
            productRepository.updateProduct(product)
        }
    }

    fun addProductToCart(product: CartProduct){
        viewModelScope.launch {
            cartRepository.addProductToCart(product)
        }
    }

    fun updateProductOfCart(product: CartProduct){
        viewModelScope.launch {
            cartRepository.updateProductOfCart(product)
        }
    }

    fun deleteProductFromCart(product: CartProduct){
        viewModelScope.launch {
            cartRepository.deleteProductFromCart(product)
        }
    }
}