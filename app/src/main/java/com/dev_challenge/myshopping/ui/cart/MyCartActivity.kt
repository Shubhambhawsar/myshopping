package com.dev_challenge.myshopping.ui.cart

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev_challenge.myshopping.R
import com.dev_challenge.myshopping.databinding.ActivityMyCartBinding
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.entities.CartProduct
import com.dev_challenge.myshopping.db.entities.Product
import com.dev_challenge.myshopping.ui.main.MainActivity
import com.dev_challenge.myshopping.ui.main.MainViewModel
import com.dev_challenge.myshopping.ui.main.ProductListAdapter
import com.example.myapplication.showToast
import com.example.myapplication.ui.CartViewModelFactory
import com.example.myapplication.ui.MainViewModelFactory

class MyCartActivity : AppCompatActivity(), ResultListener {
    lateinit var binding: ActivityMyCartBinding
    lateinit var viewModel: CartViewModel
    lateinit var adapter : CartProductListAdapter
    var cartProductsList  = ArrayList<CartProduct>()
    lateinit var productList : List<Product>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityMyCartBinding>(this, R.layout.activity_my_cart)
        binding.lifecycleOwner = this
        viewModel = ViewModelProvider(this, CartViewModelFactory(application, this, this)).get(CartViewModel::class.java)
        initialise();
    }

    private fun initialise() {
        adapter = CartProductListAdapter(this, cartProductsList);
        binding.apply {
            rvCartItems.layoutManager = LinearLayoutManager(this@MyCartActivity)
            rvCartItems.adapter = adapter

            ivBtnBack.setOnClickListener {
                startActivity(Intent(this@MyCartActivity, MainActivity::class.java))
                finish()
            }
        }
        addProductnCartListObservers()
    }

    private fun calculateCharges() {
        var subTotal = 0
        var deliveryFee = 0
        var vat = 0.0
        var total = 0.0
        cartProductsList.map {
            subTotal += (it.price * it.count)
        }
        if (subTotal<200) {
            deliveryFee = 20
            vat = subTotal * 0.2
        }

        total = subTotal + deliveryFee + vat

        binding.apply {
            txvSubTotal.setText(subTotal.toString() + " \u20A8")
            txvDeliveryFee.setText(deliveryFee.toString() + " \u20A8")
            txvVat.setText("%.2f".format(vat) + " \u20A8")
            txvToTal.setText(total.toString() + " \u20A8")
        }
    }

    private fun addProductnCartListObservers() {
        viewModel.allProductsInCart.observe(this, Observer {
            cartProductsList.clear()
            it.map {
                cartProductsList.add(it)
            }
            adapter.notifyDataSetChanged()
            calculateCharges()
        })

        viewModel.allProductsInDb.observe(this, Observer {
            productList = it
        })
    }

    fun updateCartProduct(clickFor: String, position: Int) {
        var itemAvailable = false
        productList.map {
            if (it.id == cartProductsList[position].productId && it.availability>0) {
                itemAvailable = true
            }
        }

        if (itemAvailable && clickFor.equals("add")) {
                val item = cartProductsList[position]
                item.count = (item.count + 1)
                viewModel.updateProductOfCart(item)
                updateProductAvailabiliy(clickFor, item)
        } else if (clickFor.equals("sub")) {
            val item = cartProductsList[position]
            item.count = (item.count - 1)
            viewModel.updateProductOfCart(item)
            updateProductAvailabiliy(clickFor, item)
        } else
            showToast("Sorry no more item available in stock")
    }

    private fun updateProductAvailabiliy(onAction:String, item: CartProduct) {
        lateinit var product: Product
        productList.map {
            if (item.productId == it.id)
                product = it
        }

        if (onAction.equals("add"))
            product.availability = (product.availability - 1)
        else if (onAction.equals("sub"))
            product.availability = (product.availability + 1)

        viewModel.updateProduct(product)
    }

    fun deleteCartProduct(position: Int) {
        showAlertDialog(cartProductsList[position])
    }

    private fun showAlertDialog(product: CartProduct) {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this)
        alertDialog.setMessage("Do you want to delete "+product.name + " from cart ?")
        alertDialog.setPositiveButton(
            "yes"
        ) { _, _ ->
            viewModel.deleteProductFromCart(product)
            updateProductAvailabiliy("sub", product)
        }
        alertDialog.setNegativeButton(
            "No"
        ) { _, _ -> }
        val alert: AlertDialog = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    override fun onSuccess(list: List<Product>?, requestType: String) {
        if (requestType.equals("add"))
            showToast("Product added in cart")
        else if (requestType.equals("update"))
            showToast("Product count updates in cart")
        else if (requestType.equals("delete"))
            showToast("Product is removed from cart")
    }

    override fun onFailure(message: String) {

    }

    override fun onBackPressed() {
        startActivity(Intent(this@MyCartActivity, MainActivity::class.java))
        finish()
    }
}