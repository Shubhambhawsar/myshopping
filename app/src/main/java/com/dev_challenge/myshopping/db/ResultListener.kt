package com.dev_challenge.myshopping.db

import com.dev_challenge.myshopping.db.entities.Product

interface ResultListener {
    fun onSuccess(
        list: List<Product>?,
        requestType: String
    )
    fun onFailure(message: String )
}