package com.example.myapplication.ui

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.repository.ProductRepository
import com.dev_challenge.myshopping.ui.main.MainViewModel

class MainViewModelFactory(
    private val application: Application,
    private val context: Context,
    private val resultListener: ResultListener
) : ViewModelProvider.NewInstanceFactory()  {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel( application,context, resultListener) as T
    }
}