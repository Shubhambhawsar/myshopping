package com.dev_challenge.myshopping.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Product(
    val image: String,
    val name: String,
    val price: Int,
    val count: Int,
    val stock: Int,
    var availability: Int,
    val offer: String,
    @PrimaryKey(autoGenerate = true) val id: Int? = null
)