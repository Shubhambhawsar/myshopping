package com.dev_challenge.myshopping.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.dev_challenge.myshopping.db.entities.CartProduct
import com.dev_challenge.myshopping.db.entities.Product

@Dao
interface MyCartDao {

    @Insert(onConflict = REPLACE)
    suspend fun addProductToCart(product: CartProduct):Long

    @Update
    suspend fun updateProductOfCart(product: CartProduct)

    @Delete
    suspend fun deleteProductFromCart(product: CartProduct)

    @Query("SELECT * FROM CartProduct")
    fun getAllProduct(): LiveData<List<CartProduct>>

    @Query("DELETE FROM CartProduct")
    suspend fun deleteAllCartProducts()

}