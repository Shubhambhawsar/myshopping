package com.example.myapplication.ui

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev_challenge.myshopping.db.ResultListener
import com.dev_challenge.myshopping.db.repository.ProductRepository
import com.dev_challenge.myshopping.ui.cart.CartViewModel
import com.dev_challenge.myshopping.ui.main.MainViewModel

class CartViewModelFactory(
    private val application: Application,
    private val context: Context,
    private val resultListener: ResultListener
) : ViewModelProvider.NewInstanceFactory()  {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CartViewModel( application,context, resultListener) as T
    }
}